Client
发送RPC：随机向一个kvserver进行请求，如果是leader则请求成功，否则被请求的kvserver返回一个leaderID，让Client继续请求
发送请求后记录timeout

同一时刻只能同时执行和等待一个操作 锁保护

KVserver
相应RPC，构造op传给raft节点
Each server should execute Op commands as Raft commits them

难点：实现线性语义，即提交过的command，展现出来的应该就是那个时候的结果

To achieve linearizability in Raft, servers must filter out duplicate requests. 
The basic idea is that servers save the results of client operations and use them to skip executing the same request multiple times.

给每个client一个uniqueID，给每个command一个proposalID，server保存被提交的command的执行结果
如果收到高的proposalID，清空之前的保存结果。模仿TCP的ack机制

client发送RPC请求的时候，附带clientID和commandId

clientID -> (commandId -> value)
acked map[clientId]commandId  // 保存记录已经被applied 但是client未作回复的commandID（即目前为止，收到最大的commandID请求）
map[clientID]map[commandID]chan string

server 开启不断接受ApplyMsg的goroutine

server接受RPC请求时
判断这个command是否已经被applied
如果是
    找到已经得到的command结果，返回
如果不是
    Start这个command
    如果当前server不是leader
        返回
    如果当前server是leader
        判断是否是新的client
        如果是，增加记录 clientId 到 Command数组的映射
        等待结果
        如果超时则返回
        返回结果

Basic3A
遇到了一些问题，在client使用args和reply的时候，
for循环中需要使用一个新的reply结构体，传递给rpc，否则还是之前的reply结果

server中，只有leader需要在RPC的等待channel上发送返回response

Unreliable3A
处理当前的在start时是leader
One way to do this is for the server to detect that it has lost leadership,
 by noticing that a different request has appeared at the index returned by Start(), 
 or that Raft's term has changed. 

在applyMsgReceiver中增加rf.GetState后，发现卡在这里
取消在这里的leadership判断
在RPC中进行判断

TestOnePartition3A不通过
有时不通过TestUnreliable3A 
似乎 多append了一个值

    2022/07/13 11:50:13 kvserver 1: finish sending
    2022/07/13 11:50:13 get wrong value, key 0, wanted:
    x 0 0 yx 0 1 yx 0 2 y
    , got
    x 0 0 yx 0 1 yx 0 1 yx 0 2 y
    exit status 1

再理解一下顺序一致性，我之前写的Memo用来保存GET第一次请求时的value，但是其实这不是顺序一致性。
顺序一致性应该保证write操作之执行一次，多次的读请求要用最新的状态机来返回。

TestOnePartition3A 卡在了选出partition的leader后
发现是client的逻辑有问题，在call返回false的时候，忘记更新leaderid

TestManyPartitionsOneClient3A failed
key 0的
仍然是对command的重复执行理解不到位，对于PUT、APPEND操作，状态集应该记录是否被操作过。
而对于冗余的GET操作，永远让客户端看到最新的状态

				ackedCmdId, ok := kv.acked[command.ClientId]
				if command.OpType != GET && (!ok || ackedCmdId < command.CommandId) {
					kv.acked[command.ClientId] = command.CommandId
					cmdRes = kv.executeCommand(command)
				} else if command.OpType == GET {
					cmdRes.Value = kv.storage[command.Key]
				} else {
					DPrintf("kvserver %d: find duplicate applyMsg, Command: %v", kv.me, command)
				}

$ go test -run 3A                                          
Test: one client (3A) ...
  ... Passed --  15.7  5   396   74
Test: many clients (3A) ...
  ... Passed --  18.1  5   765  365
Test: unreliable net, many clients (3A) ...
  ... Passed --  24.0  5   959  179
Test: concurrent append to same key, unreliable (3A) ...
  ... Passed --   6.4  3   150   52
Test: progress in majority (3A) ...
  ... Passed --   1.5  5    53    2
Test: no progress in minority (3A) ...
  ... Passed --   1.6  5    77    3
Test: completion after heal (3A) ...
  ... Passed --   1.2  5    41    3
Test: partitions, one client (3A) ...
  ... Passed --  24.1  5   621   46
Test: partitions, many clients (3A) ...
  ... Passed --  25.5  5  1019  220
Test: restarts, one client (3A) ...
labgob warning: Decoding into a non-default variable/field int may not work
  ... Passed --  22.9  5  1096   70
Test: restarts, many clients (3A) ...
  ... Passed --  25.0  5  1733  375
Test: unreliable net, restarts, many clients (3A) ...
  ... Passed --  30.1  5  2015  151
Test: restarts, partitions, many clients (3A) ...
  ... Passed --  31.9  5  1534  159
Test: unreliable net, restarts, partitions, many clients (3A) ...
  ... Passed --  33.1  5  1775   97
Test: unreliable net, restarts, partitions, many clients, linearizability checks (3A) ...
  ... Passed --  32.7  7  4532  293
PASS
ok      _/Users/sjy/develop/Go/6.824/src/kvraft 296.354s



Part B: Key/value service with log compaction
每个server独立地命令raft进行日志压缩，当kv.rf.persistor.RaftStateSize() > kv.maxraftstate的时候，
server告诉raft节点进行日志丢弃，同时传递给raft snapshot

kv.snapshotting() 被定时调用，检查raftstate的大小，通知raft进行日志丢弃，

Raft结构增加startIndex，以后的log的真实的index通过接口来访问
rf.installSnapshot(snapshot Snapshot, index int)
增加raft节点获取log的接口，便于修改raft RPChandler

rf.LogAtIndex()
rf.getLogTailIndex()  // startIndex + len(rf.log)，返回最后一个有效log的下一个下标
rf.trimLogStartIndex()

how to restore the snapshot when kvserver restart ?
在test_test.go中，发现在调用StartServer后，这个server重新启动，
config.go中StartServer方法里，会选择已经被保存了状态的persister进行初始化一个kvserver
所以需要调整server.go中的StartKVServer，读取persister的状态

todo: test the log with startIndex
pass 2A, 2B, 2C
raft需要读取snapshot来获取元信息

todo: test installSnapshot

task: implement InstallSnapshot RPC
when to send? when conflict index less than the startIndex

follower installed snapshot后，怎么更新状态机？
用applyCh，给ApplyMsg增加field

重新测试raft
TestFailNoAgree2B，发现是在sendingHeartBeat中，双重加锁了
同时，sendingHeartBeat在等待多个RPC回复的时候，进行处理之前还得判断是不是leader
AppendEntries RPC handler中，查询conflict index的时候，设定最低log index为startIndex

lab3A rerun, pass

test lab3B
在raft根据每个server的nextIndex发送log的时候，有些follower的nextindex可能小于rf.startIndex，
这时候调整convLogIndex，在遇到这种情况的时候，返回0下标，让sendingheartbeat继续运行，当收到回复的时候，再进行sendInstallSnapshot

$ go test -run 3B                                                                                                                              1 ↵
Test: InstallSnapshot RPC (3B) ...
  ... Passed --  17.2  3   295   63
Test: snapshot size is reasonable (3B) ...
--- FAIL: TestSnapshotSize3B (161.40s)
    config.go:65: test took longer than 120 seconds
Test: restarts, snapshots, one client (3B) ...
labgob warning: Decoding into a non-default variable/field int may not work
  ... Passed --  23.0  5  1117   70
Test: restarts, snapshots, many clients (3B) ...
  ... Passed --  34.2  5  4103 1480
Test: unreliable net, snapshots, many clients (3B) ...
  ... Passed --  32.8  5  1111  158
Test: unreliable net, restarts, snapshots, many clients (3B) ...
  ... Passed --  36.8  5  2426  174
Test: unreliable net, restarts, partitions, snapshots, many clients (3B) ...
--- FAIL: TestSnapshotUnreliableRecoverConcurrentPartition3B (9.81s)
    test_test.go:86: 0 missing element x 0 1 y in Append result x 0 0 yx 0 3 y
Test: unreliable net, restarts, partitions, snapshots, many clients, linearizability checks (3B) ...
  ... Passed --  32.7  7  4306  288
FAIL
exit status 1
FAIL    _/Users/sjy/develop/Go/6.824/src/kvraft 349.560s

有两个失败的测试
在kvserver更新状态机的时候加锁保护，通过了TestSnapshotUnreliableRecoverConcurrentPartition3B
但是TestSnapshotSize3B总是耗费时间在160s，大于要求的120s

─$ go test -run 3B
Test: InstallSnapshot RPC (3B) ...
  ... Passed --  18.8  3   409   63
Test: snapshot size is reasonable (3B) ...
--- FAIL: TestSnapshotSize3B (161.48s)
    config.go:65: test took longer than 120 seconds
Test: restarts, snapshots, one client (3B) ...
labgob warning: Decoding into a non-default variable/field int may not work
  ... Passed --  22.8  5  1111   74
Test: restarts, snapshots, many clients (3B) ...
  ... Passed --  35.2  5  4156 1480
Test: unreliable net, snapshots, many clients (3B) ...
  ... Passed --  23.6  5   933  189
Test: unreliable net, restarts, snapshots, many clients (3B) ...
  ... Passed --  27.0  5  1860  197
Test: unreliable net, restarts, partitions, snapshots, many clients (3B) ...
  ... Passed --  35.4  5  1807   89
Test: unreliable net, restarts, partitions, snapshots, many clients, linearizability checks (3B) ...
  ... Passed --  34.2  7  4541  258
FAIL
exit status 1

但出现偶尔不通过
go test -run TestSnapshotUnreliableRecoverConcurrentPartition3B
Test: unreliable net, restarts, partitions, snapshots, many clients (3B) ...
labgob warning: Decoding into a non-default variable/field int may not work
--- FAIL: TestSnapshotUnreliableRecoverConcurrentPartition3B (23.26s)
    test_test.go:86: 3 missing element x 3 0 y in Append result x 3 1 yx 3 2 y
FAIL
exit status 1
FAIL    _/Users/sjy/develop/Go/6.824/src/kvraft 23.425s

7.27
把rf.startIndex也加入到persist中要保存的状态里
可以每次都通过TestSnapshotUnreliableRecoverConcurrentPartition3B测试了

7.29 但似乎还是存在不能通过的时候
在安装完snapshot之后，增加更新lastApplied和commitIndex
在raft中增加encodeRaftState，通过一次性加锁完成persister.SaveStateAndSnapshot
raft测试发现速度没什么大问题，所以说raft的实现没什么问题

发现kvraft的测试3A速度测试比较慢，官网上是237s，我是266s
kvraft的client轮训等待时间调整为10 millisecond
在raft follower节点收到snapshot后，对于部分丢弃日志，也传递给kv层新的snapshot
还是存在错误和超时

$ go test -run 3B
Test: InstallSnapshot RPC (3B) ...
  ... Passed --  16.5  3   550   63
Test: snapshot size is reasonable (3B) ...
--- FAIL: TestSnapshotSize3B (161.41s)
    config.go:65: test took longer than 120 seconds
Test: restarts, snapshots, one client (3B) ...
labgob warning: Decoding into a non-default variable/field int may not work
  ... Passed --  20.6  5  1763   77
Test: restarts, snapshots, many clients (3B) ...
  ... Passed --  32.9  5 24512 1524
Test: unreliable net, snapshots, many clients (3B) ...
  ... Passed --  19.0  5  1326  356
Test: unreliable net, restarts, snapshots, many clients (3B) ...
--- FAIL: TestSnapshotUnreliableRecover3B (15.63s)
    test_test.go:86: 2 missing element x 2 6 y in Append result x 2 0 yx 2 1 yx 2 2 yx 2 3 yx 2 4 yx 2 5 yx 2 7 yx 2 8 yx 2 9 yx 2 10 yx 2 11 y
Test: unreliable net, restarts, partitions, snapshots, many clients (3B) ...
  ... Passed --  32.3  5  2876  190
Test: unreliable net, restarts, partitions, snapshots, many clients, linearizability checks (3B) ...
  ... Passed --  27.0  7  6409  302
FAIL
exit status 1
FAIL    _/Users/sjy/develop/Go/6.824/src/kvraft 326.041s