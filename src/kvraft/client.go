package kvraft

import (
	"../labrpc"
	"crypto/rand"
	// mrand "math/rand"
	"math/big"
	"time"
)


type Clerk struct {
	servers []*labrpc.ClientEnd
	// You will have to modify this struct.
	clientId int64   // unique client ID
	commandId int64  // Monotonically increasing
	leader int
}

func nrand() int64 {
	max := big.NewInt(int64(1) << 62)
	bigx, _ := rand.Int(rand.Reader, max)
	x := bigx.Int64()
	return x
}

func MakeClerk(servers []*labrpc.ClientEnd) *Clerk {
	ck := new(Clerk)
	ck.servers = servers
	// You'll have to add code here.
	ck.clientId = nrand()
	ck.commandId = 0
	ck.leader = (int)(nrand()) % len(servers)
	return ck
}

//
// fetch the current value for a key.
// returns "" if the key does not exist.
// keeps trying forever in the face of all other errors.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("KVServer.Get", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) Get(key string) string {
	args := GetArgs{Key: key, ClientId: ck.clientId, CommandId: ck.commandId}
	serverNum := len(ck.servers)
	ck.commandId += 1
	findCount := 0
	for {
		server := ck.servers[ck.leader]
		reply := GetReply{}  // need a fresh reply
		if ok := server.Call("KVServer.Get", &args, &reply); ok {
			if reply.Err == "" {
				DPrintf("Clerk %d :Get [%s] %s success.\n", ck.clientId % 100, key, reply.Value)
				findCount = 0
				return reply.Value
			} else if reply.Err == ErrWrongLeader {
				DPrintf("Clerk %d Get failed: %s, choose another server%d\n",ck.clientId % 100, reply.Err, ck.leader)
			} else {
				DPrintf("Clerk %d Get failed: %s\n",ck.clientId % 100, reply.Err)
				continue
			}
		}
		ck.leader = (ck.leader + 1) % serverNum
		findCount += 1
		if findCount == serverNum {
			time.Sleep(10 * time.Millisecond)
			findCount = 0
		}
	}
}

//
// shared by Put and Append.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("KVServer.PutAppend", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) PutAppend(key string, value string, op string) {
	// You will have to modify this function.
	args := PutAppendArgs{Key: key, Value: value, Op: op, ClientId: ck.clientId, CommandId: ck.commandId}
	serverNum := len(ck.servers)
	// serverIndex := nrand() % serverNum
	ck.commandId += 1
	findCount := 0
	for {
		server := ck.servers[ck.leader]
		reply := PutAppendReply{}
		// DPrintf("Clerk %d: sending Putappend request",  ck.clientId % 100)
		if ok := server.Call("KVServer.PutAppend", &args, &reply); ok {
			if reply.Err == "" {
				DPrintf("Clerk %d: %s [%s]<=%s success.\n", ck.clientId % 100, op, key, value)
				findCount = 0
				return
			} else if reply.Err == ErrWrongLeader {
				DPrintf("Clerk %d: %s failed, Err: %s",  ck.clientId % 100, op, reply.Err)
			} else {
				DPrintf("Clerk %d: %s failed: %s\n",ck.clientId % 100, op, reply.Err)
				continue
			}
		}
		ck.leader = (ck.leader + 1) % serverNum
		findCount += 1
		if findCount == serverNum {
			time.Sleep(time.Millisecond * 10)
			findCount = 0
		}
	}
}

func (ck *Clerk) Put(key string, value string) {
	ck.PutAppend(key, value, "Put")
}
func (ck *Clerk) Append(key string, value string) {
	ck.PutAppend(key, value, "Append")
}
