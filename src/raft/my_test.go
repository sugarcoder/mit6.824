package raft

import (
	"bytes"
	"io/ioutil"
	"os"
	"time"
	"sync"
	"testing"
	"fmt"
	"../labgob"
)

type stringSlice []string

// stringSlice is reference
func (p stringSlice) swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func TestSlice(t *testing.T) {
	peers := []string{"abc" ,"123", "edf"}
	for i := range peers {
		fmt.Printf("%d ", i)
	}
	arr := []string{}
	arr = append(arr, peers[3:3]...)
	fmt.Println(arr)
	// fmt.Println()
	// stringSlice(peers).swap(0, 1)
	// for _, v := range peers {
	// 	fmt.Printf("%s ", v)
	// }
	// fmt.Println()
	// for i := range(peers) {
	// 	fmt.Println(i)
	// }
}

func TestCond(t *testing.T) {
	var mu sync.Mutex
	myCond := sync.NewCond(&mu)
	count := 0
	
	go func() {
		fmt.Println("I'm waiting signal...")
		myCond.L.Lock()
		for count < 2 {
			fmt.Println("Not enought, waiting...")
			myCond.Wait()
		}
		myCond.L.Unlock()
		fmt.Printf("I finish! count = %d\n", count)
	}()

	go func() {
		fmt.Println("I go to sleep.")
		for i := 0; i < 3; i++ {
			time.Sleep(time.Millisecond * 1000)
			count++
			myCond.Signal()
			fmt.Printf("Signal %d\n", i)
		}
		fmt.Println("Signaler finish...")
	}()

	time.Sleep(time.Second * 4)
	fmt.Println("Main routine finish")
}

func TestTempfile(t *testing.T) {
	file, _ := ioutil.TempFile(".", "tp")
	fmt.Fprintf(file, "new file?")
	
	os.Rename(file.Name(), "test.txt") // rename会覆盖
}

func TestAppend(t *testing.T) {
	logs := make([]string, 0, 16)
	fmt.Printf("len(logs) = %d\n", len(logs))
	logs = append(logs, "hello")
	logs = append(logs, "world")
	fmt.Printf("len(logs) = %d\n", len(logs))
	fmt.Println(logs[1])
}

func TestSince(t *testing.T) {
	t1 := time.Now()
	for time.Since(t1).Seconds() < 2 {
		fmt.Println("Wait")
		time.Sleep(time.Millisecond * 500)
	}
}

type raft struct {
	Role int
	Message interface{}
	Log []logEntry
}

type logEntry struct {
	Command interface{}
	Term    int // 这条logEntry是在term时期被写入的
}

func TestLabEncode(t *testing.T) {
	r := raft{12, "follower", []logEntry{logEntry{"x->1", 1}, logEntry{123, 3}}}
	var disk bytes.Buffer
	encoder := labgob.NewEncoder(&disk)
	decoder := labgob.NewDecoder(&disk)

	encoder.Encode(r)
	// encoder.Encode(r.Message)
	// data := disk.Bytes()
	// fmt.Println(data)

	var q raft
	decoder.Decode(&q)
	fmt.Printf("%v\n", q)
	var m string
	decoder.Decode(&m)
	fmt.Println(m)
}

func TestGoRoutine(t *testing.T) {
	ch := make(chan int)
	go func() {
		fmt.Println("Routine1 start sleeping for 1 seconds...")
		time.Sleep(time.Duration(time.Second * 1))
		ch <- 1
		fmt.Println("Routine1 finished")
	}()

	go func() {
		fmt.Println("Routine 2 start sleeping for 1 seconds...")
		time.Sleep(time.Duration(time.Second * 1))
		ch <- 1
		fmt.Println("Routine2 finished")
	}()
	timer := time.NewTimer(time.Duration(time.Second * 3))

	select {
	case <-ch:
		fmt.Println("Main routine got the ch msg")
	case <-timer.C:
		fmt.Println("Timer stop")
	}
	// <-ch
	timer.Reset(time.Duration(time.Second * 3))
	select {
	case <-ch:
		fmt.Println("Got signal")
	case <-timer.C:
		fmt.Println("timer stop")
	}
}